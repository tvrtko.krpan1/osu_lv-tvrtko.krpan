import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import svm

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.pipeline import Pipeline
from sklearn.pipeline import make_pipeline
from sklearn.model_selection import GridSearchCV


#1)


def plot_decision_regions(X, y, classifier, resolution=0.02):
    plt.figure()
    # setup marker generator and color map
    markers = ('s', 'x', 'o', '^', 'v')
    colors = ('red', 'blue', 'lightgreen', 'gray', 'cyan')
    cmap = ListedColormap(colors[:len(np.unique(y))])
    
    # plot the decision surface
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
    np.arange(x2_min, x2_max, resolution))
    Z = classifier.predict(np.array([xx1.ravel(), xx2.ravel()]).T)
    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)
    plt.xlim(xx1.min(), xx1.max())
    plt.ylim(xx2.min(), xx2.max())
    
    # plot class examples
    for idx, cl in enumerate(np.unique(y)):
        plt.scatter(x=X[y == cl, 0],
                    y=X[y == cl, 1],
                    alpha=0.8,
                    c=colors[idx],
                    marker=markers[idx],
                    label=cl)


# ucitaj podatke
data = pd.read_csv("Social_Network_Ads.csv")
print(data.info())

data.hist()
#plt.show()

# dataframe u numpy
X = data[["Age","EstimatedSalary"]].to_numpy()
y = data["Purchased"].to_numpy()

# podijeli podatke u omjeru 80-20%
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.2, stratify=y, random_state = 10)

# skaliraj ulazne velicine
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform((X_test))

# Model logisticke regresije
LogReg_model = LogisticRegression(penalty=None) 
LogReg_model.fit(X_train_n, y_train)
knn_model = KNeighborsClassifier(n_neighbors=5)
knn_model.fit(X_train_n, y_train)

# Evaluacija modela logisticke regresije
y_train_p = LogReg_model.predict(X_train_n)
y_test_p = LogReg_model.predict(X_test_n)
y_train_pred_knn = knn_model.predict(X_train_n)
y_test_pred_knn = knn_model.predict(X_test_n)


print("Logisticka regresija: ")
print("Tocnost train: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
print("Tocnost test: " + "{:0.3f}".format((accuracy_score(y_test, y_test_p))))
print("KNN (K=5) - Točnost na skupu za učenje:", accuracy_score(y_train, y_train_pred_knn))
print("KNN (K=5) - Točnost na skupu za testiranje:", accuracy_score(y_test, y_test_pred_knn))


# granica odluke pomocu logisticke regresije
plot_decision_regions(X_train_n, y_train, classifier=LogReg_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Tocnost: " + "{:0.3f}".format((accuracy_score(y_train, y_train_p))))
plt.tight_layout()
# plt.show()

plot_decision_regions(X_train_n, y_train, classifier=knn_model)
plt.xlabel('Age (scaled)')
plt.ylabel('EstimatedSalary (scaled)')
plt.title("Tocnost: " + "{:0.3f}".format(accuracy_score(y_train, y_train_pred_knn)))
# plt.show()

#KNN algoritam ima veću točnost nego pomocu logisticke regresije

#2)

knn_model = KNeighborsClassifier(n_neighbors=1)
knn_model.fit(X_train_n, y_train)

y_train_pred_knn = knn_model.predict(X_train_n)
y_test_pred_knn = knn_model.predict(X_test_n)

plot_decision_regions(X_train_n, y_train, classifier=knn_model)
plt.xlabel('Age (scaled)')
plt.ylabel('EstimatedSalary (scaled)')
plt.title("Tocnost: " + "{:0.3f}".format(accuracy_score(y_train, y_train_pred_knn)))
# plt.show()

knn_model = KNeighborsClassifier(n_neighbors=100)
knn_model.fit(X_train_n, y_train)

y_train_pred_knn = knn_model.predict(X_train_n)
y_test_pred_knn = knn_model.predict(X_test_n)

newknn = KNeighborsClassifier()

plot_decision_regions(X_train_n, y_train, classifier=knn_model)
plt.xlabel('Age (scaled)')
plt.ylabel('EstimatedSalary (scaled)')
plt.title("Tocnost: " + "{:0.3f}".format(accuracy_score(y_train, y_train_pred_knn)))
# plt.show()
param_grid = {"n_neighbors":  np.arange(1, 101)}
svm_gscv = GridSearchCV ( newknn , param_grid , cv =5 , scoring ="accuracy", n_jobs = -1 )
svm_gscv . fit ( X_train , y_train )
print("Optimalan broj je:", svm_gscv.best_params_['n_neighbors'])

#3)

from sklearn.svm import SVC
SVM_model = SVC(kernel = 'rbf', gamma = 1 , C = 0.1)

SVM_model.fit(X_train_n, y_train)

plot_decision_regions(X_train_n, y_train, classifier=SVM_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Granica odluke za SVM (RBF kernel)")
plt.tight_layout()
plt.show()

y_test_pred_SVM = SVM_model.predict(X_test_n)
accuracy_SVM = accuracy_score(y_test, y_test_pred_SVM)
print(f"Točnost modela na skupu podataka za testiranje: {accuracy_SVM:.3f}")

SVM_model = SVC(kernel = 'rbf', gamma = 0.01 , C = 10)

SVM_model.fit(X_train_n, y_train)

plot_decision_regions(X_train_n, y_train, classifier=SVM_model)
plt.xlabel('x_1')
plt.ylabel('x_2')
plt.legend(loc='upper left')
plt.title("Granica odluke za SVM (RBF kernel)")
plt.tight_layout()
plt.show()

y_test_pred_SVM = SVM_model.predict(X_test_n)
accuracy_SVM = accuracy_score(y_test, y_test_pred_SVM)
print(f"Točnost modela na skupu podataka za testiranje: {accuracy_SVM:.3f}")


#4)

# Pomocu unakrsne validacije odredite optimalnu vrijednost hiperparametra ´ C i γ
# algoritma SVM za problem iz Zadatka 1.

newgammac = SVC(kernel='rbf')
param_grid = {"C": [0.1 , 1 , 0.5 , 10 , 100 ], "gamma": [0.1 , 1 , 0.01 , 0.5 , 100 , 10 ]}
svm_gscv = GridSearchCV ( newgammac , param_grid , cv =5 , scoring = "accuracy",n_jobs = -1 )
svm_gscv . fit ( X_train , y_train )
print ( svm_gscv . best_params_ )