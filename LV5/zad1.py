import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, accuracy_score, precision_score, recall_score

import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification

X_train, X_test, y_train, y_test = train_test_split(data[:, 0:7], data[:, 8], test_size=0.2, random_state=5)
X_train, y_train = make_classification(n_samples=100, n_features=2, n_informative=2, n_redundant=0, n_clusters_per_class=1, random_state=0)

# Generiranje podataka za testiranje
X_test, y_test = make_classification(n_samples=100, n_features=2, n_informative=2, n_redundant=0, n_clusters_per_class=1, random_state=1)

# Razdvajanje podataka prema klasama
X0_train = X_train[y_train == 0]
X1_train = X_train[y_train == 1]

X0_test = X_test[y_test == 0]
X1_test = X_test[y_test == 1]

# Prikaži podatke za učenje
plt.scatter(X0_train[:, 0], X0_train[:, 1], c='b', label='Klasa 0 - Učenje')
plt.scatter(X1_train[:, 0], X1_train[:, 1], c='r', label='Klasa 1 - Učenje')

# Prikaži podatke za testiranje s drugim markerom
plt.scatter(X0_test[:, 0], X0_test[:, 1], c='b', marker='x', label='Klasa 0 - Testiranje')
plt.scatter(X1_test[:, 0], X1_test[:, 1], c='r', marker='x', label='Klasa 1 - Testiranje')

plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Podaci za učenje i testiranje')
plt.legend()
plt.show()



#b

# Inicijalizacija modela logističke regresije
model = LogisticRegression()
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train , y_train)

# Učenje modela na skupu podataka za učenje
model.fit(X_train, y_train)

# Ispisivanje točnosti modela na skupu za učenje
train_accuracy = model.score(X_train, y_train)
print("Točnost modela na skupu za učenje:", train_accuracy)

#c


# Pronalaženje parametara modela
theta0 = model.intercept_[0]
theta1, theta2 = model.coef_[0]

# Računanje granice odluke (x2 kao funkcija x1)
x1_values = np.linspace(min(X_train[:, 0]), max(X_train[:, 0]), 100)
x2_values = -(theta0 + theta1 * x1_values) / theta2

# Prikaz podataka za učenje
plt.scatter(X0_train[:, 0], X0_train[:, 1], c='b', label='Klasa 0 - Učenje')
plt.scatter(X1_train[:, 0], X1_train[:, 1], c='r', label='Klasa 1 - Učenje')

# Prikaz granice odluke
plt.plot(x1_values, x2_values, label='Granica odluke', color='green')

plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Granica odluke naučenog modela logističke regresije')
plt.legend()
plt.show()

#d

# Klasifikacija skupa podataka za testiranje
y_pred = model.predict(X_test)
y_test_p = LogRegression_model.predict(X_test)

# Izračun matrice zabune
conf_matrix = confusion_matrix(y_test, y_pred)
print("Matrica zabune:")
print(conf_matrix)
cm = confusion_matrix(y_test , y_test_p)
print ("Confusion matrix : " , cm )
disp = ConfusionMatrixDisplay(confusion_matrix(y_test , y_test_p))
disp.plot()
plt.show()

# Izračun točnosti, preciznosti i odziva
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
print ("Accuracy : " , accuracy_score(y_test , y_test_p))
print ("Precision : " , precision_score(y_test , y_test_p))
print ("Recall : " , recall_score(y_test , y_test_p))

print("\nTočnost:", accuracy)
print("Preciznost:", precision)
print("Odziv:", recall)


