import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

data = pd.read_csv("data_C02_emission.csv")
plt.figure()
plt.hist(data['CO2 Emissions (g/km)'], bins=20, color='skyblue', edgecolor='black')
plt.title('Histogram emisije CO2 plinova')
plt.xlabel('Emisija CO2 plinova (g/km)')
plt.ylabel('Broj vozila')
plt.grid(True)
plt.show()


plt.figure()
plt.scatter(data[:, 7], data[:, 5], s=0.5)
plt.title("Age vs. BMI")
plt.show()

print("Min BMI: ", data[:, 5].min())
print("Max BMI: ", data[:, 5].max())
print("Average BMI: ", (data[:, 5].mean()))

print("Have diabets: ")
yd = data[data[:,8] == 1]
print("Min BMI: ", yd[:, 5].min())
print("Max BMI: ", yd[:, 5].max())
print("Average BMI: ", (yd[:, 5].sum()/len(yd[:, 5])))

print("Does not have diabetes: ")
nd = data[data[:,8] == 0]
print("Min BMI: ", nd[:, 5].min())
print("Max BMI: ", nd[:, 5].max())
print("Average BMI: ", (nd[:, 5].sum()/len(nd[:, 5])))


dizel = data[data['Fuel Type'] == 'D']
benzin = data[data['Fuel Type'] == 'Z']

plt.scatter(dizel['Fuel Consumption City (L/100km)'], dizel['CO2 Emissions (g/km)'], color='blue', label='Dizel')
plt.scatter(benzin['Fuel Consumption City (L/100km)'], benzin['CO2 Emissions (g/km)'], color='red', label='Benzin')
plt.title('Odnos između gradske potrošnje goriva i emisije CO2 plinova')
plt.xlabel('Gradska potrošnja goriva (L/100km)')
plt.ylabel('Emisija CO2 plinova (g/km)')
plt.legend()
plt.grid(True)
plt.show()

plt.boxplot([dizel['Fuel Consumption Hwy (L/100km)'], benzin['Fuel Consumption Hwy (L/100km)']], 
            labels=['Dizel', 'Benzin'])
plt.title('Razdioba izvangradske potrošnje goriva s obzirom na tip goriva')
plt.xlabel('Tip goriva')
plt.ylabel('Izvangradska potrošnja goriva (L/100km)')
plt.grid(True)
plt.show()

broj_vozila_po_tipu_goriva = data.groupby('Fuel Type').size()

broj_vozila_po_tipu_goriva.plot(kind='bar', color='skyblue')
plt.title('Broj vozila po tipu goriva')
plt.xlabel('Tip goriva')
plt.ylabel('Broj vozila')
plt.xticks(rotation=0)  
plt.grid(axis='y')  
plt.show()

prosjecna_emisija_po_cilindrima = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()
print(prosjecna_emisija_po_cilindrima)
prosjecna_emisija_po_cilindrima.plot(kind='bar', color='green')
plt.title('Prosječna emisija CO2 plinova po broju cilindara')
plt.xlabel('Broj cilindara')
plt.ylabel('Prosječna emisija CO2 plinova (g/km)')
plt.xticks(rotation=0)  
plt.grid(axis='y') 
plt.show()