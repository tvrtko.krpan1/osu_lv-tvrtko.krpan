import pandas as pd
import numpy as np

data = pd.read_csv("data_C02_emission.csv")
data =  np.loadtxt("pima-indians-diabetes.csv", delimiter=',', skiprows=9)

print("Number of samples: ", len(data))
retci =  len(data)
print(f"Sadžri {retci} mjerenja")
print(f"Tipovi podataka: \n{data.dtypes}")

print("Empty age: ", len(data[data[:, 7]== 0]))
print("Empty BMI: ", len(data[data[:, 5]== 0]))
print("Identical: ", (len(data)-len(np.unique(data, axis=0))))
data = data[data[:, 5] != 0] #drop empty bmi
data = data[data[:, 7] != 0] #drop empty age
data = np.unique(data, axis=0) #drop duplicated entries
print("Number of samples after drop: ", len(data))

print("Izostale vrijednosti:")
print(data.isna().sum())

print("\nDuplicirani redovi:")
print(data.duplicated().sum())

data . dropna ( axis =0 )
data . dropna ( axis =1 )
data . drop_duplicates ()
data = data . reset_index ( drop = True )

kategoricki_stupci = ["Make", "Model", "Vehicle Class", "Transmission", "Fuel Type"]
data[kategoricki_stupci] = data[kategoricki_stupci].astype("category")

print(data.dtypes)

najmanja_potrosnja = data.sort_values(by="Fuel Consumption City (L/100km)").head(3)
najveca_potrosnja = data.sort_values(by="Fuel Consumption City (L/100km)", ascending=False).head(3)
print("Automobili s najmanjom gradskom potrošnjom:")
for index, row in najmanja_potrosnja.iterrows():
    print(f"{row['Make']}, {row['Model']}: {row['Fuel Consumption City (L/100km)']} L/100km")

print("\nAutomobili s najvećom gradskom potrošnjom:")
for index, row in najveca_potrosnja.iterrows():
    print(f"{row['Make']}, {row['Model']}: {row['Fuel Consumption City (L/100km)']} L/100km")


filtered_data = data[(data['Engine Size (L)'] > 2.5) & (data['Engine Size (L)'] < 3.5)]

broj_vozila = len(filtered_data)

avg_emision = filtered_data['CO2 Emissions (g/km)'].mean()

print(f"Broj vozila s veličinom motora između 2.5 i 3.5 L: {broj_vozila}")
print(f"Prosječna CO2 emisija za ta vozila: {avg_emision} g/km")



audi_data = data[data['Make'] == 'Audi']

broj_audi_mjerenja = len(audi_data)

audi_4_cilindra = audi_data[audi_data['Cylinders'] == 4]

prosjecna_emisija_co2_audi_4_cilindra = audi_4_cilindra['CO2 Emissions (g/km)'].mean()

print(f"Mjerenja za vozila proizvođača Audi: {broj_audi_mjerenja}")
print(f"Prosječna CO2 emisija za automobile Audi s 4 cilindra: {prosjecna_emisija_co2_audi_4_cilindra} g/km")


broj_vozila_po_cilindrima = data['Cylinders'].value_counts().sort_index()

prosjecna_emisija_po_cilindrima = data.groupby('Cylinders')['CO2 Emissions (g/km)'].mean()

print("Broj vozila po broju cilindara:")
print(broj_vozila_po_cilindrima)

print("\nProsječna CO2 emisija po broju cilindara:")
print(prosjecna_emisija_po_cilindrima)

dizel_data = data[data['Fuel Type'] == 'D']

benzin_data = data[data['Fuel Type'] == 'X']

prosjecna_gradska_dizel = dizel_data['Fuel Consumption City (L/100km)'].mean()
prosjecna_gradska_benzin = benzin_data['Fuel Consumption City (L/100km)'].mean()

medijalna_gradska_dizel = dizel_data['Fuel Consumption City (L/100km)'].median()
medijalna_gradska_benzin = benzin_data['Fuel Consumption City (L/100km)'].median()

print("Prosječna gradska potrošnja:")
print(f"Dizel: {prosjecna_gradska_dizel} L/100km")
print(f"Benzin: {prosjecna_gradska_benzin} L/100km")

print("\nMedijalna gradska potrošnja:")
print(f"Dizel: {medijalna_gradska_dizel} L/100km")
print(f"Benzin: {medijalna_gradska_benzin} L/100km")



filtrirani_podaci = data[(data['Cylinders'] == 4) & (data['Fuel Type'] == 'D')]
novi = filtrirani_podaci.sort_values(by="Fuel Consumption City (L/100km)",ascending=False).head(1)
print(novi)

rucni_mjenjac = data[data['Transmission'].str.startswith('M')]

broj_rucnih_mjenjaca = len(rucni_mjenjac)

print(f"Broj vozila s ručnim mjenjačem: {broj_rucnih_mjenjaca}")

korelacija = data.corr()

print("Korelacija između numeričkih veličina:")
print(korelacija)