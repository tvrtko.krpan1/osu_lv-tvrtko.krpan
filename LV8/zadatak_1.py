import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from tensorflow.keras.models import Sequential

# Zatim možete koristiti layers iz tensorflow.keras


# Model / data parameters
num_classes = 10
input_shape = (28, 28, 1)

# train i test podaci
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

# prikaz karakteristika train i test podataka
print('Train: X=%s, y=%s' % (x_train.shape, y_train.shape))
print('Test: X=%s, y=%s' % (x_test.shape, y_test.shape))

# TODO: prikazi nekoliko slika iz train skupa


# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

# slike trebaju biti (28, 28, 1)
x_train_s = np.expand_dims(x_train_s, -1)
x_test_s = np.expand_dims(x_test_s, -1)

print("x_train shape:", x_train_s.shape)
print(x_train_s.shape[0], "train samples")
print(x_test_s.shape[0], "test samples")


# pretvori labele
y_train_s = keras.utils.to_categorical(y_train, num_classes)
y_test_s = keras.utils.to_categorical(y_test, num_classes)

#1.1 Skup za ucenje sadrzi 60 000 primjera a testni 10 000 primjera. Ulazni podaci, odnosno slike, skalirani su na raspon [0,1]. Ovo se postiglo dijeljenjem svih vrijednosti piksela s maksimalnom vrijednošću piksela (255), budući da su pikseli inicijalno prikazani kao vrijednosti u rasponu od 0 do 255. Dakle, skaliranje je izvedeno tako da se sve vrijednosti piksela podijele s 255 kako bi se osiguralo da sve vrijednosti budu u rasponu između 0 i 1. Ovo je uobičajena praksa prilikom pripreme slika za obradu u neuronskim mrežama, jer pomaže u postizanju stabilnosti u procesu učenja.
#Izlazna velicina je kodirana pomoću one-hot kodiranja jer ima to_categoralical

#1.2 
plt.imshow(x_train[0], cmap='gray')  
plt.title('Primjer slike za učenje')
plt.axis('off')  
plt.show()
print('Oznaka slike:', y_train[0])


# TODO: kreiraj model pomocu keras.Sequential(); prikazi njegovu strukturu
#1.3
model = keras.Sequential()
model.add(layers.Input(shape =(784, )))
model.add(layers.Dense(100, activation ="relu"))
model.add(layers.Dense(50, activation ="relu"))
model.add(layers.Dense(10, activation ="softmax"))
model.summary()


# TODO: definiraj karakteristike procesa ucenja pomocu .compile()
#1.4
model.compile(optimizer='adam',  
              loss='categorical_crossentropy', 
              metrics=['accuracy'])

x_train_2 = x_train.reshape(60000, 784)
x_test_2 = x_test.reshape(10000, 784)
# TODO: provedi ucenje mreze
epochs = 1
batch_size = 32

# Pokretanje treninga mreže
history = model.fit(x_train_2, y_train_s, epochs=epochs, batch_size=batch_size, validation_split=0.1)
predictions = model.predict(x_test_2)
#1.5
score = model.evaluate( x_test_2 , y_test_s , verbose =0 )
# Ispis rezultata učenja
print('score: ', score)
y_pred =np.argmax(predictions, axis=1)
conf_mat = confusion_matrix(y_test, y_pred)
print('Confusion matrix:')
print(conf_mat)

# TODO: spremi model
model.export("LV8/nn")
del model



# TODO: Prikazi test accuracy i matricu zabune



# TODO: spremi model

data =  np.loadtxt("pima-indians-diabetes.csv", delimiter=',', skiprows=9)
X = data[:, 0:8]
y = data[:, 8]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)
#print(X)
#print(y)
#print(len(X_test))
#a)
model = keras.Sequential()
model.add(layers.Input(shape = (8, )))
model.add(layers.Dense(12, activation = "relu"))
model.add(layers.Dense(8, activation = "relu"))
model.add(layers.Dense(1, activation = "sigmoid"))
model.summary()
#b)
model.compile(loss = "binary_crossentropy", optimizer = "adam", metrics = ["accuracy", ])
#c)
batch_size = 10
epochs = 150
history = model.fit(X_train , y_train, batch_size = batch_size, epochs = epochs, validation_split = 0.1)
predictions = model.predict(X_test)
score = model.evaluate(X_test, y_test ,verbose = 0)
#d)
model.save("FCNispit1/")
del model
#e)
model = load_model("FCNispit1/")
model.summary ()

score = model.evaluate(X_test, y_test, verbose = 0)
print(score)
#f)
predictions = model.predict(X_test)
predictions = np.round(predictions)
cm = confusion_matrix(y_test , predictions)
print ("Confusion matrix : " , cm )
disp = ConfusionMatrixDisplay(confusion_matrix(y_test , predictions))
disp.plot()
plt.show()

