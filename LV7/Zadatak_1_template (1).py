import matplotlib.pyplot as plt
import numpy as np
from scipy.cluster.hierarchy import dendrogram
from sklearn.datasets import make_blobs, make_circles, make_moons
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.metrics import silhouette_score

def generate_data(n_samples, flagc):
    # 3 grupe
    if flagc == 1:
        random_state = 365
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
    
    # 3 grupe
    elif flagc == 2:
        random_state = 148
        X,y = make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)

    # 4 grupe 
    elif flagc == 3:
        random_state = 148
        X, y = make_blobs(n_samples=n_samples,
                        centers = 4,
                        cluster_std=np.array([1.0, 2.5, 0.5, 3.0]),
                        random_state=random_state)
    # 2 grupe
    elif flagc == 4:
        X, y = make_circles(n_samples=n_samples, factor=.5, noise=.05)
    
    # 2 grupe  
    elif flagc == 5:
        X, y = make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

# generiranje podatkovnih primjera
X = generate_data(500, 1)

# prikazi primjere u obliku dijagrama rasprsenja
plt.figure()
plt.scatter(X[:,0],X[:,1])
plt.xlabel('$x_1$')
plt.ylabel('$x_2$')
plt.title('podatkovni primjeri')
plt.show()

#1 imamo 3 grupe u generiranim podacima

#2 
for k in range(2, 6):
    kmeans = KMeans(n_clusters=k)
    kmeans.fit(X)
    labels = kmeans.labels_
    centers = kmeans.cluster_centers_
    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis')
    plt.scatter(centers[:, 0], centers[:, 1], c='red', s=200, alpha=0.5)
    plt.xlabel('$x_1$')
    plt.ylabel('$x_2$')
    plt.title('KMeans grupiranje')
    plt.show()
    
#3    
def optimal_k(X):
    scores = []
    for k in range(2, 11):
        kmeans = KMeans(n_clusters=k)
        kmeans.fit(X)
        score = silhouette_score(X, kmeans.labels_)
        scores.append(score)
    return np.argmax(scores) + 2  

for flagc in range(1, 6):
    X = generate_data(500, flagc)
    k = optimal_k(X)
    kmeans = KMeans(n_clusters=k)
    kmeans.fit(X)
    labels = kmeans.labels_
    centers = kmeans.cluster_centers_

    plt.figure()
    plt.scatter(X[:, 0], X[:, 1], c=labels, cmap='viridis')
    plt.scatter(centers[:, 0], centers[:, 1], c='red', s=200, alpha=0.5)
    plt.xlabel('$x_1$')
    plt.ylabel('$x_2$')
    plt.title(f'KMeans grupiranje (flagc={flagc})')
    plt.show()

    #cvijece
    import numpy as np
import pandas as pd
from tensorflow import keras
from keras import layers
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn import datasets
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from sklearn.metrics import ConfusionMatrixDisplay

#1----------------------------------------
iris = datasets.load_iris()
iris_df=pd.DataFrame(iris.data)
iris_df['class']=iris.target #0=setosa, 1=versicolor, 2=virginica
iris_df.columns=['sepal_len', 'sepal_wid', 'petal_len', 'petal_wid', 'class']
print(iris_df)

iris_df.dropna(how="all", inplace=True) # remove any empty lines
print(iris_df)

#a)
virginice_sketer=iris_df[iris_df['class']==2]
setosa_sketer=iris_df[iris_df['class']==0]
plt.scatter(setosa_sketer['petal_len'],setosa_sketer['sepal_len'],c="gray")
plt.scatter(virginice_sketer['petal_len'],virginice_sketer['sepal_len'],c="green")
plt.title("Odnos duljina latica sa duljinom čašice")
plt.xlabel("Veličina latica")
plt.ylabel("Veličina čašice")
plt.legend(["setosa","virginica"])
plt.show()
#DIjagram pokazuje odnos duljine latica i čašica za cvijet iris virginica(zeleni scatter) i iris setosa(sivi scatter)

#b)
virginica_sirina=iris_df[iris_df['class']==2]
versicolor_sirina=iris_df[iris_df['class']==1]
setosa_sirina=iris_df[iris_df['class']==0]
x=["Virginica","Versicolor","Setosa"]
y=[virginica_sirina['sepal_wid'].max(),versicolor_sirina['sepal_wid'].max(),setosa_sirina['sepal_wid'].max()]
bar_colors = ['green', 'blue','purple']
plt.bar(x,y,color=bar_colors)
plt.title("Najveća vrijednost čašice za pojedini cvijet")
plt.xlabel("Vrste")
plt.ylabel("Duljina čašice u cm")
plt.show()

#c)
setosas=iris_df[iris_df['class']==0]
broj_iznad_prosjeka=setosas[setosas['sepal_wid']>setosas['sepal_wid'].mean()]
print(f'broj Setosa koje imaju veću širinu čašice od prosjeka {setosas['sepal_wid'].max()}:{len(broj_iznad_prosjeka)}')

#2---------------------------------------
iris2 = datasets.load_iris()
iris2_df=pd.DataFrame(iris2.data)
iris2_df['class']=iris.target #0=setosa, 1=versicolor, 2=virginica
iris2_df.columns=['sepal_len', 'sepal_wid', 'petal_len', 'petal_wid', 'class']
print(iris2_df)

iris2_df.dropna(how="all", inplace=True)
print(iris2_df)

#a)
J=[]
K=[1,2,3,4,5,6,7,8]
for i in K:
    km=KMeans(n_clusters=i,init="random",n_init=5, random_state=0)
    km.fit(iris_df.iloc[:, :-1])
    J.append(km.inertia_)

#b)
plt.figure()
plt.title("Ovisnost J o K")
plt.plot(K,J,marker='o')
plt.xlabel('(K)')
plt.ylabel('(J)')
plt.title('Lakat metoda ovisnosti K o J')
plt.show()
#najoptimalniji K je 3

#c)
X=iris_df.iloc[:, :-1]
km=KMeans(n_clusters=3 ,init="random",n_init=5, random_state=0)
km.fit(X)
labels=km.predict(X)

#d)
cluster_centroids = km.cluster_centers_
color_map = {
    0: 'green',
    1: 'yellow',
    2: 'orange'
}
colors = [color_map[label] for label in km.labels_]
plt.figure()
plt.title("Ovisnosti širine čašice o duljini čašice")
plt.xlabel("Sepal length")
plt.ylabel("Sepal width")
plt.scatter(X['sepal_len'],X['sepal_wid'],c=colors)
plt.scatter(cluster_centroids[:, 0], cluster_centroids[:, 1], marker='x', c='red', s=100, label='Centroids')
plt.show()

plt.figure()
plt.title("Ovisnosti širine latica o duljini latice")
plt.xlabel("Petal length")
plt.ylabel("Petal width")
plt.scatter(X['petal_len'],X['petal_wid'],c=colors)
plt.scatter(cluster_centroids[:, 2], cluster_centroids[:, 3], marker='x', c='red', s=100, label='Centroids')
plt.show()

#e)
#3------------------------------------
iris2 = datasets.load_iris()
iris2_df=pd.DataFrame(iris2.data)
iris2_df['class']=iris2.target #0=setosa, 1=versicolor, 2=virginica
iris2_df.columns=['sepal_len', 'sepal_wid', 'petal_len', 'petal_wid', 'class']
print(iris2_df)

X = iris2_df[['sepal_len', 'sepal_wid', 'petal_len', 'petal_wid']].to_numpy()
y = iris2_df[['class']].to_numpy()
print(X,y)

X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.2, stratify=y, random_state=10)

y_train_encoded = to_categorical(y_train)
y_test_encoded = to_categorical(y_test)

#a)
model = keras.Sequential()
model.add(layers.Input(shape=(4,)))#U SHAPE IDE ONOLKO BROJ KOLKO IMAS STUPACA PODATAKA U XU
model.add(layers.Dense(12, activation='relu'))
model.add(layers.Dropout(0.2))
model.add(layers.Dense(7, activation='relu'))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(5, activation='relu'))
model.add(layers.Dense(3, activation='softmax'))
model.summary()

#b)
model.compile(loss="categorical_crossentropy", optimizer="adam",metrics=["accuracy",])

#c
epoch=450
batch=7
history=model.fit(X_train, y_train_encoded, batch_size=batch, epochs=epoch, validation_split=0.1)

#d
model.save("spremljen/mojmodel.keras")
del model
#e
model=keras.models.load_model("spremljen/mojmodel.keras")
score=model.evaluate(X_test,y_test_encoded,verbose=0)

#f
predictions=model.predict(X_test)
y_pred=np.argmax(predictions,axis=1)
conf_mat=confusion_matrix(y_test,y_pred)
print(conf_mat)
disp=ConfusionMatrixDisplay(confusion_matrix=conf_mat)
disp.plot()
plt.show()
print(f"Točnost testa: {score}")
