import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans

# ucitaj sliku
img = Image.imread("imgs\\imgs\\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

kmeans = KMeans(n_clusters=5, n_init=10)  
kmeans.fit(img_array)

colors = kmeans.cluster_centers_

num_colors = len(np.unique(colors, axis=0))

print("Broj različitih boja prisutnih u slici:", num_colors)

kmeans.fit(img.reshape(-1, 3))  
colors = kmeans.cluster_centers_

num_colors = len(np.unique(colors, axis=0))

print("Broj različitih boja prisutnih u slici:", num_colors)

labels = kmeans.predict(img.reshape(-1, 3))

centers = kmeans.cluster_centers_

new_img = centers[labels].reshape(w, h, d)

plt.figure()
plt.title("Rezultantna slika")
plt.imshow(new_img)
plt.tight_layout()
plt.show()


inertia_values = []

k_values = range(1, 11)  
for k in k_values:
    kmeans = KMeans(n_clusters=k, random_state=42,n_init=10)
    kmeans.fit(img.reshape(-1, 3))  
    
    inertia_values.append(kmeans.inertia_)

plt.plot(k_values, inertia_values, marker='o')
plt.xlabel('Broj grupa K')
plt.ylabel('Inercija')
plt.title('Ovisnost inercije o broju grupa K')
plt.xticks(k_values)
plt.grid(True)
plt.show()
