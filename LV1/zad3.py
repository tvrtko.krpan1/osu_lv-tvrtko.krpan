numbers = []

while True:
    number = input("Unesite broj ili done za prekid: ")
    if number == "Done":
        break
    try:
        number_ = float(number)
    except:
        print("unos mora biti broj")
        continue

    numbers.append(number_)

count = len(numbers)
total = sum(numbers)
minimum = min(numbers)
maximum = max(numbers)
average = total / count

print(f"Uneseno je {count} brojeva")
print(f"Srednja vrijednost je {average}")
print(f"Maksimalna vrijednost je {maximum}")
print(f"Minimalna vrijednosti je {minimum}")