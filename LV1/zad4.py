""" Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci 
koje se pojavljuju u ˇ
datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) 
pojavljuje u datoteci. ˇ
Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih. """

with open("song.txt") as song:
    song = song.read()
song = song.lower()
song = song.replace(",", "")
words = song.split()

word_count = {}
for word in words:
    if word in word_count:
        word_count[word] += 1
    else:
        word_count[word] = 1

unique_words = [word for word, count in word_count.items() if count == 1]

print("Broj riječi koje se ponavljajuju samo jednom je: ", len(unique_words))

for word in unique_words:
    print(word)
