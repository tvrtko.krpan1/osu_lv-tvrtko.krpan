def load_sms_collection():
    ham_messages = []
    spam_messages = []
    spam_with_exclamation = 0

    with open("SMSSpamCollection.txt") as file:
        for line in file:
            label, message = line.strip().split('\t')
            if label == 'ham':
                ham_messages.append(message)
            elif label == 'spam':
                spam_messages.append(message)
                if message.endswith('!'):
                    spam_with_exclamation += 1
    
    total_words = sum(len(message.split()) for message in spam_messages )

    avg = int(total_words / len(spam_messages))
    """ print("Prosječan broj riječi u porukama koje su tipa ham:", avg_word_count_ham) """
    print("Prosječan broj riječi u porukama koje su tipa spam:", avg)
    print("Broj SMS poruka koje su tipa spam, a završavaju uskličnikom:", spam_with_exclamation)

load_sms_collection()