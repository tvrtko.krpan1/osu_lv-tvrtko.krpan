

def ocjena():
    try:
        mark = float(input("Unesite broj između 0 i 1: "))
        if(mark < 0 or mark > 1):
            raise ValueError("Broj mora biti između 0 i 1")
        if(mark >= 0.9):
            print("A")
        if(mark >= 0.8):
            print("B")
        if(mark >= 0.7):
            print("C")
        if(mark >= 0.6):
            print("D")
        else:
            print("F")    
    except:
        print("Error occured")

ocjena()

