import numpy as np
import matplotlib . pyplot as plt

data = np.loadtxt('data.csv', skiprows=1, delimiter=',')

print(f"Mjerenja su izvršena na {data.shape[0]} ljudi")

spol = data[:, 0]    
visina = data[:, 1]  
masa = data[:, 2]    

plt.scatter(visina, masa)
plt.xlabel("Visina")
plt.ylabel("Masa")
plt.title("Odnos mase i visine")
plt.show()

visina_svakih_50 = data[::50,1]
masa_svakih_50 = data[::50,2]

plt.scatter(visina_svakih_50,masa_svakih_50)
plt.xlabel("Visina")
plt.ylabel("Masa")
plt.title("Odnos mase i visine")
plt.show()

print(f"Minimalna vrijednost visine osobe: {visina.min()}")
print(f"Maksimalna vrijednost visine osobe: {visina.max()}")
print(f"Srednja vrijednost visine osobe: {visina.mean()}")

muskarci = data[:,0] == 1
zene = data[:,0] == 0

visina_muskaraca = data[muskarci,1]
visina_zena = data[zene,1]

min_visina_muškarci = np.min(visina_muskaraca)
max_visina_muškarci = np.max(visina_muskaraca)
srednja_visina_muškarci = np.mean(visina_muskaraca)

min_visina_žene = np.min(visina_zena)
max_visina_žene = np.max(visina_zena)
srednja_visina_žene = np.mean(visina_zena)

print(f"Minimalna vrijednost visine muškarca: {min_visina_muškarci}")

print(f"Maksimalna vrijednost visine muškarca: {max_visina_muškarci}")

print(f"Srednja vrijednost visine muškarca: {srednja_visina_muškarci}")

print(f"Minimalna vrijednost visine žena: {min_visina_žene}")

print(f"Maksimalna vrijednost visine žena: {max_visina_žene}")

print(f"Srednja vrijednost visine žena: {srednja_visina_žene}")
