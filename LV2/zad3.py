""" Zadatak 2.4.3 Skripta zadatak_3.py ucitava 
sliku ’road.jpg’. Manipulacijom odgovarajuce
numpy matrice pokušajte:
a) posvijetliti sliku,
b) prikazati samo drugu cetvrtinu slike po širini,
c) zarotirati sliku za 90 stupnjeva u smjeru 
kazaljke na satu,
d) zrcaliti sliku. """

import numpy as np
import matplotlib . pyplot as plt
img = plt . imread ("road.jpg")
image = img [:,:,0]. copy ()

plt . figure ()
plt.imshow(img, alpha=0.5, cmap="grey")
plt . show ()

width = image.shape[1]

start_col = width // 4  
end_col = width // 2    

plt.figure()
plt.imshow(image[:, start_col:end_col])
plt.show()

rotated_img = np.rot90(image, k=-1)
plt.imshow(rotated_img)
plt.show()

mirrored_img = np.flip(image, axis=(0, 1))

# Prikazivanje zrcaljene slike
plt.imshow(mirrored_img)
plt.show()