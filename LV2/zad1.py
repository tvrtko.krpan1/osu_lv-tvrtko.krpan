import numpy as np
import matplotlib . pyplot as plt

a = np.array([1,2])
b = np.array([1,2])
c = np.array([2,3])
d = np.array([2,2])
e = np.array([3,3])
f = np.array([1,2])
g = np.array([1,3])
h = np.array([1,1])
plt . plot (a,b, "r", linewidth =1, marker =".", markersize =5)
plt . plot (c,d, "b", linewidth =0.5, marker =".", markersize =5)
plt . plot (e,f, "g", linewidth =0.5, marker =".", markersize =5)
plt . plot (g,h, "y", linewidth =0.5, marker =".", markersize =5)
plt . axis ([0,4,0,4])
plt . xlabel ("x os")
plt . ylabel ("y os")
plt . show ()