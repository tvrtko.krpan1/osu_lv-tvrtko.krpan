import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.utils import to_categorical
from matplotlib import pyplot as plt


# ucitaj CIFAR-10 podatkovni skup
(X_train, y_train), (X_test, y_test) = cifar10.load_data()

# prikazi 9 slika iz skupa za ucenje
plt.figure()
for i in range(9):
    plt.subplot(330 + 1 + i)
    plt.xticks([]),plt.yticks([])
    plt.imshow(X_train[i])

plt.show()


# pripremi podatke (skaliraj ih na raspon [0,1]])
X_train_n = X_train.astype('float32')/ 255.0
X_test_n = X_test.astype('float32')/ 255.0

# 1-od-K kodiranje
y_train = to_categorical(y_train)
y_test = to_categorical(y_test)

# CNN mreza
model = keras.Sequential()
model.add(layers.Input(shape=(32,32,3)))
model.add(layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Conv2D(filters=128, kernel_size=(3, 3), activation='relu', padding='same'))
model.add(layers.MaxPooling2D(pool_size=(2, 2)))
model.add(layers.Flatten())
model.add(layers.Dense(500, activation='relu'))
model.add(layers.Dropout(0.3))
model.add(layers.Dense(10, activation='softmax'))

model.summary()

# definiraj listu s funkcijama povratnog poziva
my_callbacks = [
    keras.callbacks.TensorBoard(log_dir = '/log/cnn_droput',
                                update_freq = 100),
    keras.callbacks.EarlyStopping(monitor ="val_loss", patience = 5 , verbose = 1, mode='min')


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sklearn . linear_model as lm
import seaborn as sns
import math
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error, r2_score, max_error
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay
from matplotlib.colors import ListedColormap
from tensorflow import keras
from keras import layers



data = pd.read_csv("winequality-red.csv",delimiter=";")
#print(data)
df=pd.DataFrame(data)
#print(df)

#1--------------------------------------------------

#a)
print(len(df))

#b)
plt.figure()
plt.bar(df['quality'],df['alcohol'])
plt.title('Ovisnost kvalitete vina o jakosti')
plt.ylabel('Jakost u promilima')
plt.xlabel('kvaliteta(ocjena)')
plt.show()

#c)
kvalitetav=df[df['quality']>=5]
kvalitetam=df[df['quality']<5]
print("manje:",len(kvalitetam))
print("veće:",len(kvalitetav))

#d)
print(df.corr())
plt.matshow(df.corr(),cmap="Greens")
plt.show()
sns.heatmap(df.corr(),annot=True,cmap="Greens")
plt.show()


#2--------------------------------------------------

X=df.drop(["quality"],axis=1)
y=df["quality"]

#print(X,"UWAAAAAAAAAWICA",y)
for i in range(len(df["quality"])):
    if df.at[i, "quality"] >= 5:
        df.at[i, 'quality'] = 1
    else:
        df.at[i, 'quality'] = 0

X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.2, stratify=y, random_state=10)
print(y_test)
print(X_test)

#a)
linearModel = lm.LinearRegression ()
linearModel.fit(X_train , y_train )
print(linearModel.coef_)

#b)
y_test_p = linearModel.predict(X_test)
plt.scatter(y_test, y_test_p,c="Yellow",s=4)
plt.xlabel('Actual Values (y_test)')
plt.ylabel('Predicted Values (y_test_p)')
plt.title('Actual vs Predicted')
plt.show()

#c)
MSE = mean_squared_error(y_test, y_test_p)
print("Mean squared error: " + str(MSE))
RMSE = math.sqrt(MSE)
print("Root mean squared error: " + str(RMSE))
MAE = mean_absolute_error( y_test , y_test_p )
print("Mean absolute error: " + str(MAE))
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print("Mean absolute precentage error: " + str(MAPE))
rsq = r2_score(y_test, y_test_p)
print("R2: " + str(rsq))

#3--------------------------------------------------
X=df.drop(["quality"],axis=1)
y=df["quality"]

for i in range(len(df["quality"])):
    if df.at[i, "quality"] >= 5:
        df.at[i, 'quality'] = 1
    else:
        df.at[i, 'quality'] = 0

X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.2, stratify=y, random_state=10)

#vin

data = pd.read_csv("winequality-red.csv",delimiter=";")
#print(data)
df=pd.DataFrame(data)
#print(df)

#1


print(len(df))


plt.figure()
plt.bar(df['quality'],df['alcohol'])
plt.title('Ovisnost kvalitete vina o jakosti')
plt.ylabel('Jakost u promilima')
plt.xlabel('kvaliteta(ocjena)')
plt.show()


kvalitetav=df[df['quality']>=5]
kvalitetam=df[df['quality']<5]
print("manje:",len(kvalitetam))
print("veće:",len(kvalitetav))


print(df.corr())
plt.matshow(df.corr(),cmap="Greens")
plt.show()
sns.heatmap(df.corr(),annot=True,cmap="Greens")
plt.show()




X=df.drop(["quality"],axis=1)
y=df["quality"]


for i in range(len(df["quality"])):
    if df.at[i, "quality"] >= 5:
        df.at[i, 'quality'] = 1
    else:
        df.at[i, 'quality'] = 0

X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.2, stratify=y, random_state=10)
print(y_test)
print(X_test)


linearModel = lm.LinearRegression ()
linearModel.fit(X_train , y_train )
print(linearModel.coef_)


y_test_p = linearModel.predict(X_test)
plt.scatter(y_test, y_test_p,c="Yellow",s=4)
plt.xlabel('Actual Values (y_test)')
plt.ylabel('Predicted Values (y_test_p)')
plt.title('Actual vs Predicted')
plt.show()


MSE = mean_squared_error(y_test, y_test_p)
print("Mean squared error: " + str(MSE))
RMSE = math.sqrt(MSE)
print("Root mean squared error: " + str(RMSE))
MAE = mean_absolute_error( y_test , y_test_p )
print("Mean absolute error: " + str(MAE))
MAPE = mean_absolute_percentage_error(y_test, y_test_p)
print("Mean absolute precentage error: " + str(MAPE))
rsq = r2_score(y_test, y_test_p)
print("R2: " + str(rsq))


X=df.drop(["quality"],axis=1)
y=df["quality"]

for i in range(len(df["quality"])):
    if df.at[i, "quality"] >= 5:
        df.at[i, 'quality'] = 1
    else:
        df.at[i, 'quality'] = 0

X_train, X_test, y_train, y_test=train_test_split(X,y,test_size=0.2, stratify=y, random_state=10)


model = keras.Sequential()
model.add(layers.Input(shape=(11,)))#U SHAPE IDE ONOLKO BROJ KOLKO IMAS STUPACA PODATAKA U XU
model.add(layers.Dense(20, activation='relu'))
model.add(layers.Dense(12, activation='relu'))
model.add(layers.Dense(4, activation='relu'))
model.add(layers.Dense(1, activation='sigmoid'))
model.summary()


model.compile(loss="binary_crossentropy", optimizer="adam",metrics=["accuracy",])


epoch=1000
batch=50
history=model.fit(X_train, y_train, batch_size=batch, epochs=epoch, validation_split=0.1)


model.save("spremljen/mojmodel.keras")
del model


model=keras.models.load_model("spremljen/mojmodel.keras")
score=model.evaluate(X_test,y_test,verbose=0)


predictions=model.predict(X_test)
y_pred=np.argmax(predictions,axis=1)
conf_mat=confusion_matrix(y_test,y_pred)
print(conf_mat)
disp=ConfusionMatrixDisplay(confusion_matrix=conf_mat)
disp.plot()
plt.show()
print(f"Točnost testa: {score}")



       
]

model.compile(optimizer='adam',
                loss='categorical_crossentropy',
                metrics=['accuracy'])

model.fit(X_train_n,
            y_train,
            epochs = 40,
            batch_size = 64,
            callbacks = my_callbacks,
            validation_split = 0.1)


score = model.evaluate(X_test_n, y_test, verbose=0)
print(f'Tocnost na testnom skupu podataka: {100.0*score[1]:.2f}')

#1
#Zadatak Broj slojeva je 10.
#model.summary()
#ne radi terminal
#nemogu ni 1.3 jer ne radi prethodni

#2
#bolje su performanse s dropout-om

#3
#Dodajte funkciju povratnog poziva za rano zaustavljanje koja ´ce zaustaviti proces
#uˇcenja nakon što se 5 uzastopnih epoha ne smanji prosjeˇcna vrijednost funkcije gubitka na
#validacijskom skupu.

#4
""""Jako velika ili jako mala veličina serije:
Jako velika veličina serije: Može dovesti do sporijeg učenja i potencijalno veće memorije koju zahtijeva proces, ali može poboljšati stabilnost gradijentnog pada i pomoći u postizanju konvergencije. Međutim, može dovesti i do prekomjerne memorije potrebne za treniranje, što može uzrokovati poteškoće ako imate ograničene resurse.
Jako mala veličina serije: Može ubrzati učenje jer se gradijentni koraci ažuriraju češće, ali može dovesti do nestabilnosti učenja i poteškoća u konvergenciji. Također, mala serija može uzrokovati prenaučenost, posebno ako imate malen skup podataka.
Jako mala ili jako velika vrijednost stope učenja:
Jako mala stopa učenja: Može dovesti do sporošću učenja ili zaglavljivanja u lokalnom minimumu, jer gradijentni koraci mogu biti premali za pokretanje procesa optimizacije. Međutim, može pomoći u postizanju stabilnog učenja i smanjenju rizika od prenaučenosti.
Jako velika stopa učenja: Može uzrokovati nestabilnosti u učenju i skakanje oko minimuma funkcije gubitka, što otežava konvergenciju. Također, može uzrokovati divergenciju gubitka.
Izbacivanje određenih slojeva iz mreže kako biste dobili manju mrežu:
Izbacivanje određenih slojeva može dovesti do gubitka složenosti i kapaciteta modela. To može rezultirati manjom mrežom koja zahtijeva manje resursa za treniranje i predikciju, ali isto tako može smanjiti sposobnost modela da nauči složenije obrasce u podacima.
Ako za 50% smanjite veličinu skupa za učenje:
Smanjenje veličine skupa za učenje može dovesti do gubitka informacija i smanjenja kapaciteta modela da nauči složene obrasce. To može uzrokovati prenaučenost, posebno ako je originalni skup podataka mali. Također, može dovesti do nestabilnosti učenja ako su podaci neravnomjerno raspoređeni ili ako su važni uzorci izostavljeni iz manjeg skupa podataka."""



