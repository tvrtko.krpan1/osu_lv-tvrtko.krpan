import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MinMaxScaler
import sklearn.linear_model as lm
import sklearn.metrics as metrics
from sklearn.preprocessing import OneHotEncoder
from sklearn import datasets


import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import sklearn . linear_model as lm
import seaborn as sns
import math
from sklearn . model_selection import train_test_split
from sklearn . preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV, cross_val_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import mean_absolute_percentage_error, r2_score, max_error
from sklearn.metrics import confusion_matrix
from sklearn.metrics import ConfusionMatrixDisplay
from matplotlib.colors import ListedColormap



#a
data = pd.read_csv('data.csv')
input_variables = ['Fuel Consumption City (L/100km)',
                    'Fuel Consumption Hwy (L/100km)',
                    'Fuel Consumption Comb (L/100km)',
                    'Fuel Consumption Comb (mpg)',
                    'Engine Size (L)',
                    'Cylinders']
output_variables = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
y = data[output_variables].to_numpy()

X , y = datasets.load_diabetes ( return_X_y = True )
X_train , X_test , y_train , y_test = train_test_split(X , y , test_size = 0.2 , random_state =1 )

#b
for i in range(len(X_train[1])):
    plt.scatter(X_train[:, i], y_train, s=1, c='b', label = 'train_data')
    plt.scatter(X_test[:, i],y_test, s = 1, c='r', label = 'test data')
    plt.xlabel("input variable")
    plt.ylabel("CO2 emission (g/km)")
    plt.legend()
    plt.show()

#c
sc = MinMaxScaler()
X_train_n = sc.fit_transform(X_train)
X_test_n = sc.transform(X_test)

fig2, axes = plt.subplots(2,1)
axes[0].hist(X_train[:,2], bins = 5)
axes[1].hist(X_train_n[:,2], bins = 5)
plt.show()

#d
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

#e
y_test_p = linearModel.predict(X_test_n)

fig3,axes2 = plt.subplots(2,1)
axes2[0].scatter(X_test_n[:, 0], y_test_p)
axes2[1].scatter(X_test_n[:, 0], y_test)
axes2[0].set_xlabel('Engine Size (L)')
axes2[0].set_ylabel('CO2 Emissions (g/km) predicted')
axes2[1].set_xlabel('Engine Size (L)')
axes2[1].set_ylabel('CO2 Emissions (g/km) real')
plt.show()

#f
RSE = metrics.mean_squared_error(y_test, y_test_p)
RMSE = metrics.mean_squared_error(y_test, y_test_p, squared = 1)