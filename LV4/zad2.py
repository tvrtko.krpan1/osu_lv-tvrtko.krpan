# Import potrebnih biblioteka
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error

data = pd.read_csv('data.csv')

input_variables = ['Fuel Consumption City (L/100km)',
                   'Fuel Consumption Hwy (L/100km)',
                   'Fuel Consumption Comb (L/100km)',
                   'Fuel Consumption Comb (mpg)',
                   'Engine Size (L)',
                   'Cylinders',
                   'Fuel Type']  
output_variables = ['CO2 Emissions (g/km)']

encoder = OneHotEncoder(sparse=False)
fuel_type_encoded = encoder.fit_transform(data[['Fuel Type']])

X = data[input_variables[:-1]].values
X = np.concatenate((X, fuel_type_encoded), axis=1)
y = data[output_variables].values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=1)

linear_model = LinearRegression()
linear_model.fit(X_train, y_train)

y_test_pred = linear_model.predict(X_test)

max_error = max(abs(y_test - y_test_pred))
print("Maksimalna pogreška u procjeni emisije CO2 plinova u g/km:", max_error)


